# for Intel programming project

## War Game infor:

    - https://en.wikipedia.org/wiki/War_(card_game)

## Version:

    - Python 3.8.1

## Usage:

    - to run the tests: use `python -m unittest`
    - to run the main game demo: use `python .\main.py`

## Corner cases:

    - one corner case is when player1 and player2 wins cards alternatively and the game never ends (that's why a STEPS_LIMIT is given in the main.py)
    - another one is when both players has cards with the same ranks and in the same order, after all the wars currently player2 wins (to be improved)

## assumptions:

    - when player runs out of cards, that player immediately loses
    - limit the max steps to be 1000, no one wins after the limit

## to improve:

    - define customized exceptions instead of build-in Error types
    - add logging (debug print) levels to filter out prints
    - add abstract 'Game' class, so other games can use the same interface
    - handle the corner case 1 mentioned above
    - add more tests to cover more logics
    - use pydoc to generate documentation

## stretch goal:

    - add > 2 players
    - implement 'Casino War'
