""" This is the Player class 
    Author: Dawei Zhang
    Date: 03/26/2021
"""


class Player:
    def __init__(self, name, deck):
        """ initialize the player with a empty hand of cards
            @param name: the name of the player
            @param deck: the init deck of cards the player holds
        """
        self._name = name
        self._hand = deck

    def lefts(self):
        """ @return: how many cards left within the player's hand
        """
        return self._hand.size()

    def drawOne(self):
        """ draw one card from the player's hand
            @return: return the card popped from the hand
        """
        return self._hand.pop() if self.lefts() else None

    def drawMultiple(self, count):
        """ draw multiple cards from the player's hand, when there is any
            @return: return a list of cards max(count, lefts) popped from the hand
        """
        res = []
        while count > 0:
            card = self.drawOne()
            if not card:
                break
            res.append(card)
            count -= 1
        return res

    def collect(self, deck):
        """ collect cards from a deck
        """
        self._hand.insert(deck.cards())

    def __str__(self):
        return '{} with {} cards'.format(self._name, self._hand.size())
