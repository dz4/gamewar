""" This is the War Card Game class
    Author: Dawei Zhang
    Date: 03/26/2021
"""

from cards.card import Card
from cards.deck import Deck
from players.player import Player


class War:
    def __init__(self, name1, name2):
        """ initialize the game with players and a fresh deck of cards
        """
        deck = Deck.newDeck()
        deck.shuffle()
        half1, half2 = deck.split()
        self._player1 = Player(name1, half1)
        self._player2 = Player(name2, half2)

    def player1(self):
        """ @return: the player1
        """
        return self._player1

    def player2(self):
        """ @return: the player2
        """
        return self._player2

    def isEnd(self):
        """ query if the game is end (when one player has no more card)
            @return: True / False
        """
        return self._player1.lefts() == 0 or self._player2.lefts() == 0

    def winner(self):
        """ get the winner Player of the game
            @return: a Player or None if the game is not end yet
        """
        if self.isEnd():
            return self._player1 if self._player1.lefts() else self._player2
        return None

    def next(self, pool):
        """ play the next step, based on the rule of War game
            @param pool: the Deck of cards on the table (not in players' hands)
        """
        if self.isEnd():
            return
        card1 = self._player1.drawOne()
        card2 = self._player2.drawOne()
        pool.insert([card1, card2])
        if card1 > card2:
            print('Players [ {}, {} ], p1 collects {} cards by {} > {}'.format(
                self._player1, self._player2, pool.size(), card1, card2))
            self._player1.collect(pool)
            pool.clear()
        elif card1 < card2:
            print('Players [ {}, {} ], p2 collects {} cards by {} > {}'.format(
                self._player1, self._player2, pool.size(), card2, card1))
            self._player2.collect(pool)
            pool.clear()
        else:
            print('War! {} == {}'.format(card1, card2))
            cards1 = self._player1.drawMultiple(3)
            cards2 = self._player2.drawMultiple(3)
            pool.insert(cards1)
            pool.insert(cards2)
