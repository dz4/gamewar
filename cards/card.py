""" This is the Card class representing a card within 52 cards
    Author: Dawei Zhang
    Date: 03/25/2021
"""

SUITS = 'D C H S'.split()  # D - Diamond, C - Club, H - Heart, S - Spade
SUIT_STRINGS = 'Diamond Club Heart Spade'.split()
RANKS = '2 3 4 5 6 7 8 9 10 J Q K A'.split()  # higher rank has bigger index


class Card:
    def __init__(self, suit, rank):
        """ @param rank: the input rank string, one of '2,3,4,5,6,7,8,9,10,J,Q,K,A'
            @param suit: the input suit string, one of 'D,C,H,S'
        """
        if rank not in RANKS:
            raise ValueError("input rank '{}' is invalid".format(rank))
        if suit not in SUITS:
            raise ValueError("input suit '{}' is invalid".format(suit))
        self._rank = rank
        self._suit = suit

    def rank(self):
        """ Get the rank order of the card
            @return: integer 0 to 12 for '2 3 4 5 6 7 8 9 10 J Q K A'
        """
        return RANKS.index(self._rank)

    def suit(self):
        """ Get the suit string of the card
            @return: one string (char) within 'D C H S'
        """
        return self._suit

    def __str__(self):
        """ the string representation of the card, e.g. '(Diamond 2)'
        """
        return '({} {})'.format(SUIT_STRINGS[SUITS.index(self._suit)], self._rank)

    def __eq__(self, other):
        """ Card's rank determines
        """
        if not isinstance(other, Card):
            return NotImplemented
        return other.rank() == self.rank()

    def __lt__(self, other):
        """ Card's rank determines, '2' is the minimum
        """
        if not isinstance(other, Card):
            return NotImplemented
        return self.rank() < other.rank()

    def __gt__(self, other):
        """ Card's rank determines, 'A' is the maximum
        """
        if not isinstance(other, Card):
            return NotImplemented
        return self.rank() > other.rank()
