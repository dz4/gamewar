"""
The cards module

This module includes following classes:
  - Card: a single Card with suit and rank
  - Deck: a set of Cards with utility functions

"""
