""" This is the Deck class representing a deck of 52 cards
    it can be used in many card games
    Author: Dawei Zhang
    Date: 03/25/2021
"""

from random import shuffle
from .card import Card, RANKS, SUITS


class Deck:
    def __init__(self, cards=[]):
        """ initialize the deck with input cards
        """
        self._validateCardsList(cards)
        self._cards = cards[:]

    def _validateCardsList(self, cards):
        if not isinstance(cards, list):
            raise ValueError('input cards is not a list')
        if cards and not all(isinstance(card, Card) for card in cards):
            raise ValueError('input cards does not contain all Card type')

    @staticmethod
    def newDeck():
        return Deck([Card(suit, rank) for suit in SUITS for rank in RANKS])

    def size(self):
        """ get the number of cards in the deck
            @return: integer of the size
        """
        return len(self._cards)

    def cards(self):
        """ get all the cards within the deck
            @return: a list of Cards
        """
        return self._cards[:]

    def clear(self):
        self._cards[:] = []

    def split(self):
        """ split the card deck into 2 half evenly, 0-size//2, size//2-end
            @return: the first and second half copy of the deck
        """
        halfSize = len(self._cards) // 2
        return Deck(self._cards[:halfSize]), Deck(self._cards[halfSize:])

    def shuffle(self):
        """ randomize the cards order
        """
        shuffle(self._cards)

    def pop(self):
        """ remove the top Card from the deck and return it
            @return: the top Card
        """
        return self._cards.pop() if len(self._cards) else None

    def insert(self, cards):
        """ insert Cards to the bottom of the deck
            @param cards: the list of Cards to insert
        """
        self._validateCardsList(cards)
        self._cards = cards + self._cards
