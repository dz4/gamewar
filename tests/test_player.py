import unittest
from cards.card import Card, SUITS, RANKS
from cards.deck import Deck
from players.player import Player


class PlayerTest(unittest.TestCase):

    def setUp(self):
        self.player = Player('Dave', Deck.newDeck())

    def test_player_lefts_should_work(self):
        self.assertEqual(self.player.lefts(), 52)

    def test_player_drawOne_should_pop_the_top(self):
        card = self.player.drawOne()
        self.assertEqual(Card(SUITS[-1], RANKS[-1]), card)
        self.assertEqual(self.player.lefts(), 51)

    def test_player_drawMultiple_should_work(self):
        remains = 52
        for count in [3, 5, 7]:
            cards = self.player.drawMultiple(count)
            self.assertEqual(len(cards), count)
            remains -= count
            self.assertEqual(self.player.lefts(), remains)

    def test_player_collect_should_work(self):
        p2 = Player('Joe', Deck())
        deck = Deck.newDeck()
        p2.collect(deck)
        self.assertEqual(52, p2.lefts())

    def test_player_string_format_should_work(self):
        self.assertEqual('{}'.format(self.player), 'Dave with 52 cards')
