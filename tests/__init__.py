"""
The tests module

This module includes following classes:
  - CardTest: the test suite for the Card class
  - DeckTest: the test suite for the Deck class
  - PlayerTest: the test suite for the Player class
  - WarTest: the test suite for the War class

"""
