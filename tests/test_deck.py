import unittest
from cards.deck import Deck
from cards.card import Card, SUITS, RANKS


class DeckTest(unittest.TestCase):

    def setUp(self):
        self.deck = Deck.newDeck()

    def test_size_of_deck_should_be_52_for_newDeck(self):
        self.assertEqual(self.deck.size(), 52)

    def is_ascending_cards(self, cards):
        i = 0
        for suit in SUITS:
            for rank in RANKS:
                if not Card(suit, rank) == cards[i]:
                    return False
                i += 1
        return True

    def test_init_deck_should_be_in_ascending_order(self):
        cards = self.deck.cards()
        self.assertTrue(self.is_ascending_cards(cards))

    def test_split_should_cut_2_halves(self):
        half1, half2 = self.deck.split()
        self.assertEqual(half1.size(), 26)
        self.assertEqual(half2.size(), 26)
        cards1, cards2 = half1.cards(), half2.cards()
        for i, card in enumerate(self.deck.cards()):
            if i < 26:
                self.assertEqual(cards1[i], card)
            else:
                self.assertEqual(cards2[i-26], card)

    def test_size_of_deck_should_be_same_with_input_cards(self):
        quarter = self.deck.split()[0].split()[0]
        self.assertEqual(quarter.size(), 13)

    def test_shuffle_should_break_the_ascending_order(self):
        self.deck.shuffle()
        cards = self.deck.cards()
        self.assertFalse(self.is_ascending_cards(cards))

    def test_pop_should_return_the_top_card(self):
        card = self.deck.pop()
        self.assertEqual(Card(SUITS[-1], RANKS[-1]), card)

    def test_pop_from_empty_deck_should_return_None(self):
        deck = Deck([Card(SUITS[0], RANKS[0])])
        deck.pop()
        card = deck.pop()
        self.assertIsNone(card)

    def test_insert_one_card_should_increase_size_by_one(self):
        self.deck.insert([Card(SUITS[0], RANKS[0])])
        self.assertEqual(53, self.deck.size())

    def test_insert_one_card_should_insert_to_bottom(self):
        self.deck.insert([Card(SUITS[0], RANKS[0])])
        cards = self.deck.cards()
        refCard = Card(SUITS[0], RANKS[0])
        self.assertEqual(refCard, cards[0])
        self.assertEqual(refCard, cards[1])

    def test_insert_invalid_card_should_fail(self):
        with self.assertRaises(ValueError):
            self.deck.insert(self.deck)

    def test_clear_should_work(self):
        self.deck.clear()
        self.assertEqual(0, self.deck.size())
