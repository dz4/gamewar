import unittest
from cards.card import Card, RANKS, SUITS


class CardTest(unittest.TestCase):

    def test_valid_rank_and_suit_should_work(self):
        for rank in RANKS:
            for suit in SUITS:
                try:
                    card = Card(suit, rank)
                except:
                    self.fail(
                        "Card raises Exception with inputs {}, {}".format(suit, rank))

    def test_invalid_suit_should_fail(self):
        with self.assertRaises(ValueError):
            card = Card('P', RANKS[0])

    def test_invalid_rank_should_fail(self):
        with self.assertRaises(ValueError):
            card = Card(SUITS[0], '1')

    def test_get_rank_should_work(self):
        card = Card(SUITS[0], RANKS[0])
        self.assertEqual(card.rank(), 0, 'card rank should be 0')
        card = Card(SUITS[0], RANKS[-1])
        self.assertEqual(card.rank(), 12, 'card rank should be 12')

    def test_get_suit_should_work(self):
        card = Card(SUITS[0], RANKS[0])
        self.assertEqual(card.suit(), 'D', 'card suit should be D')
        card = Card(SUITS[-1], RANKS[0])
        self.assertEqual(card.suit(), 'S', 'card suit should be S')

    def test_card_string_should_work(self):
        card = Card(SUITS[0], RANKS[0])
        self.assertEqual('{}'.format(card), '(Diamond 2)',
                         'card string should be (Diamond 2)')
        card = Card(SUITS[-1], RANKS[-1])
        self.assertEqual('{}'.format(card), '(Spade A)',
                         'card string should be (Spade A)')

    def test_card_equal_should_work(self):
        card = Card(SUITS[0], RANKS[0])
        for suit in SUITS[1:]:
            card2 = Card(suit, RANKS[0])
            self.assertEqual(card, card2)

    def test_card_lessThan_should_work(self):
        prev = Card(SUITS[0], RANKS[0])
        for rank in RANKS[1:]:
            card = Card(SUITS[0], rank)
            self.assertTrue(prev < card)
            prev = card

    def test_card_greaterThan_should_work(self):
        prev = Card(SUITS[0], RANKS[0])
        for rank in RANKS[1:]:
            card = Card(SUITS[0], rank)
            self.assertTrue(card > prev)
            prev = card
