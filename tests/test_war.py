import unittest

from cards.deck import Deck
from games.war import War


class WarTest(unittest.TestCase):

    def setUp(self):
        self.game = War('Dave', 'Joe')

    def test_war_init_should_not_end(self):
        self.assertFalse(self.game.isEnd())

    def test_war_init_2_players_shoud_both_have_26_cards(self):
        self.assertEqual(self.game.player1().lefts(), 26)
        self.assertEqual(self.game.player2().lefts(), 26)

    def test_war_collect_should_work(self):
        deck = Deck(self.game.player2().drawMultiple(26))
        self.game.player1().collect(deck)
        self.assertEqual(self.game.player1().lefts(), 52)

    def test_war_p1_should_be_winner_when_p2_run_out_of_cards(self):
        deck = Deck(self.game.player2().drawMultiple(26))
        self.game.player1().collect(deck)
        self.assertTrue(self.game.isEnd())
        self.assertEqual(self.game.winner(), self.game.player1())

    def test_war_p2_should_be_winner_when_p1_run_out_of_cards(self):
        deck = Deck(self.game.player1().drawMultiple(26))
        self.game.player2().collect(deck)
        self.assertTrue(self.game.isEnd())
        self.assertEqual(self.game.winner(), self.game.player2())

    def test_each_step_players_should_collect_cards(self):
        pool = Deck()
        p1Cards = self.game.player1().lefts()
        p2Cards = self.game.player2().lefts()
        for _ in range(10):
            self.game.next(pool)
            p1CardsNew = self.game.player1().lefts()
            p2CardsNew = self.game.player2().lefts()
            self.assertNotEqual(p1Cards, p1CardsNew)
            self.assertNotEqual(p2Cards, p2CardsNew)
            self.assertEqual(p1CardsNew + p2CardsNew + pool.size(), 52)
            p1Cards, p2Cards = p1CardsNew, p2CardsNew
