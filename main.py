from cards.deck import Deck
from games.war import War

STEPS_LIMIT = 1000

war = War('Dave', 'Joe')

pool = Deck()
step = 0
while not war.isEnd() and step < STEPS_LIMIT:
    war.next(pool)
    step += 1

if war.isEnd():
    war.winner().collect(pool)
    print('the winner is {} after {} steps'.format(war.winner(), step))
else:
    print('no winner after {} steps'.format(STEPS_LIMIT))
